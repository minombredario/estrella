window.addEventListener("load",eventWindowLoaded,false);

var imagenes = [];
var data = "";
var cont = 1;
var radio = "";
var N_estrellas = "";

function eventWindowLoaded(){

    miCanvasApp();

}


function miCanvasApp(){
    
   var canvas = document.getElementById("miCanvas");
    
   //comprobación si el navegador soporta canvas
    if(!canvas.getContext){
        
        return;
    }
        
    var ctx = canvas.getContext("2d");
    
    // calcula la anchura y la altura del canvas
    var ojbCanvas = getComputedStyle(canvas);
    var w = ojbCanvas.width
    var h = ojbCanvas.height;

    // extrae el valor numerico
    var W = canvas.width = w.split('px')[0];
    var H = canvas.height = h.split('px')[0];

    // calcula las coordenadas del centro y el radio
    var X = Math.floor(W/2);
    var Y = Math.floor(H/2);

    //dibujo el lienzo
    ctx.fillStyle = "#000";
    ctx.arc(X,Y,X,0,2*Math.PI);
    ctx.fill();


    document.getElementById("dibujar").addEventListener("click",function(){
        dibujar(ctx,canvas,X,Y);
    });

    document.getElementById("borrar").addEventListener("click",function(){
        limpiar(ctx,X,Y);
    });
    document.getElementById("guardar").addEventListener("click",function(){
        guardar(data);
    });


}

function guardar(data){

    if(data == ""){
        avisos(radio,N_estrellas,data);
    }else{
        var dataImg=data.toDataURL();
        imagenes.push(dataImg);

        var filas = Math.ceil(imagenes.length/4);

        CrearFilas(filas);
        CrearThumb(filas);
        CargarImagenes();
    }


}

function CrearFilas(filas){

    if(document.getElementById("fil-" + filas)){

        return;

    }else{
        var node =document.createElement("div");
        node.setAttribute("class","row");
        node.setAttribute("id", "fil-" + filas);
        node.setAttribute("class","row");
        document.getElementById("galeria").appendChild(node);
    }

}

function CrearThumb(filas){

    if(document.getElementById("fil-" + filas)){
        var node =document.createElement("div");
        node.setAttribute("class","col-xs-6 col-md-3");
        node.setAttribute("id", "col-" + cont);
        document.getElementById("fil-" + filas).appendChild(node);
        cont++;
    }else{
        return;
    }

}

function CargarImagenes(){

    document.getElementById("col-" + (cont-1)).innerHTML= "<a href='"+imagenes[(cont-2)]+"' target='_blank' class='thumbnail'><img src='"+imagenes[(cont-2)]+"'</a>";

}

function limpiar(ctx,X,Y){

    ctx.fillStyle = "#000";
    ctx.arc(X,Y,X,0,2*Math.PI);
    ctx.fill();

    document.getElementById("radio").value = "";//diametro es radio*2
    document.getElementById("cantidad").value = "";
    document.getElementById("color").value = "#000000";
    data = "";
}



function dibujar(ctx,canvas,X,Y){

 //calculo los puntos donde iran los vertices de la estrella

    radio = document.getElementById("radio").value;//diametro es radio*2
    N_estrellas = document.getElementById("cantidad").value;
    var color = document.getElementById("color").value;

    avisos(radio,N_estrellas,data);

    //dibujo la estrella
    var posicion = 2*Math.PI/5/N_estrellas; //calculo la posicion del vertice.

    var ptos = [];
    for(var i = 0; i < N_estrellas; i++){
        if(i==0){
            ptos = calcularPuntos(X,Y,radio,i);
        }else{
            ptos = calcularPuntos(X,Y,radio,i * posicion);
        }

        ctx.save();
        if(color == "#000000"){
            ctx.strokeStyle = aleatorio();
        }else{
            ctx.strokeStyle = color;
        }

        ctx.beginPath();

            ctx.moveTo(ptos[4][0],ptos[4][1]);
            ctx.lineTo(ptos[4][0],ptos[4][1]);
            ctx.lineTo(ptos[1][0],ptos[1][1]);
            ctx.lineTo(ptos[3][0],ptos[3][1]);
            ctx.lineTo(ptos[0][0],ptos[0][1]);
            ctx.lineTo(ptos[2][0],ptos[2][1]);
            ctx.lineTo(ptos[4][0],ptos[4][1]);
        ctx.stroke();
        ctx.restore();
        radio = radio- radio/10;//reducir el tamaño de la estrella
    }

    data = canvas;
}

function avisos(radio,N_estrellas,data){

    if ((radio == "" || N_estrellas == "")){
        if(data == ""){
            document.getElementById("aviso")
                 .className = document.getElementById("aviso")
                 .className.replace( "alert-info" , "alert-warning" );
            document.getElementById("aviso").innerHTML='No hay imagen que guardar';
        }else{
            document.getElementById("aviso")
                 .className = document.getElementById("aviso")
                 .className.replace( "alert-info" , "alert-warning" );
            document.getElementById("aviso").innerHTML='Datos incorrectos o campos vacios';
        }
    }else if(radio > 300){
        document.getElementById("aviso")
             .className = document.getElementById("aviso")
             .className.replace( "alert-info" , "alert-danger" );
        document.getElementById("aviso").innerHTML='Tamaño demasiado grande para el lienzo';
    }else{
        if(document.getElementById("aviso").className.match(/(?:^|\s)alert-warning/) ){
             document.getElementById("aviso")
             .className = document.getElementById("aviso")
             .className.replace("alert-warning","alert-info")
        }else if(document.getElementById("aviso").className.match(/(?:^|\s)alert-danger/) ){
             document.getElementById("aviso")
             .className = document.getElementById("aviso")
             .className.replace("alert-danger","alert-info")
        }



        document.getElementById("aviso").innerHTML='Elige el tamaño de la estrella y la cantidad de estrellas a dibujar | Con color negro seleccionado las estrellas serán de color aleatorio';
    }
}

function calcularPuntos(X,Y,radio,giro){
    var ptos = [];
    var puntas = 5;
    var f = Math.PI / puntas * 2;

    for (var i = 0; i < puntas; i++){
        var x = radio * Math.cos(i * f + giro) + X;
        var y = radio * Math.sin(i * f + giro) + Y;
        ptos.push([x,y]);
    }
    return ptos;
    }

function aleatorio(){
    var r = Math.floor(Math.random() * 255) + 70;
    var g = Math.floor(Math.random() * 255) + 70;
    var b = Math.floor(Math.random() * 255) + 70;
    var color = 'rgba(' + r + ',' + g + ',' + b + ', 1)';
    return color;
}
