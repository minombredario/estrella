window.addEventListener("load",eventWindowLoaded,false);

var imagenes = [];
var data = "";
var cont = 1;
var R = "";
var L = "";

function eventWindowLoaded(){

    miCanvasApp();

}


function miCanvasApp(){

   var canvas = document.getElementById("miCanvas");

   //comprobación si el navegador soporta canvas
    if(!canvas.getContext){

        return;
    }

    var ctx = canvas.getContext("2d");

    // calcula la anchura y la altura del canvas
    var ojbCanvas = getComputedStyle(canvas);
    var w = ojbCanvas.width
    var h = ojbCanvas.height;

    // extrae el valor numerico
    var W = canvas.width = w.split('px')[0];
    var H = canvas.height = h.split('px')[0];

    // calcula las coordenadas del centro y el radio
    var X = Math.floor(W/2);
    var Y = Math.floor(H/2);

    //dibujo el lienzo
    ctx.fillStyle = "#000";
    ctx.arc(X,Y,X,0,2*Math.PI);
    ctx.fill();


    document.getElementById("dibujar").addEventListener("click",function(){
        dibujar(ctx,canvas,X,Y);
    });

    document.getElementById("borrar").addEventListener("click",function(){
        limpiar(ctx,X,Y);
    });
    document.getElementById("guardar").addEventListener("click",function(){
        guardar(data);
    });


}

function guardar(data){

    if(data == ""){
        avisos(R,L,data);
    }else{
        var dataImg=data.toDataURL();
        imagenes.push(dataImg);

        var filas = Math.ceil(imagenes.length/4);

        CrearFilas(filas);
        CrearThumb(filas);
        CargarImagenes();
    }


}

function CrearFilas(filas){

    if(document.getElementById("fil-" + filas)){

        return;

    }else{
        var node =document.createElement("div");
        node.setAttribute("class","row");
        node.setAttribute("id", "fil-" + filas);
        node.setAttribute("class","row");
        document.getElementById("galeria").appendChild(node);
    }

}

function CrearThumb(filas){

    if(document.getElementById("fil-" + filas)){
        var node =document.createElement("div");
        node.setAttribute("class","col-xs-6 col-md-3");
        node.setAttribute("id", "col-" + cont);
        document.getElementById("fil-" + filas).appendChild(node);
        cont++;
    }else{
        return;
    }

}

function CargarImagenes(){

    document.getElementById("col-" + (cont-1)).innerHTML= "<a href='"+imagenes[(cont-2)]+"' target='_blank' class='thumbnail'><img src='"+imagenes[(cont-2)]+"'</a>";

}

function limpiar(ctx,X,Y){

    ctx.fillStyle = "#000";
    ctx.arc(X,Y,X,0,2*Math.PI);
    ctx.fill();

    document.getElementById("radio").value = "";//diametro es radio*2
    document.getElementById("cantidad").value = "";
    document.getElementById("color").value = "#000000";
    data = "";
}



function dibujar(ctx,canvas,X,Y){

     if (ctx) {
         ctx.fillStyle = aleatorio();//linea
         ctx.strokeStyle = aleatorio();//relleno
         ctx.lineWidth = 3;

         R = document.getElementById("radio").value;//diametro es radio*2;
         L = document.getElementById("cantidad").value;;
            // si L == 6 el ángulo es de 2π/6 o sea 60°
        var rad = (2*Math.PI)/L;
        // traslada el contexto en el centro del canvas
        // para poder girar el contexto alrededor del centro
        ctx.translate(canvas.width/2-X, canvas.height/2+Y);
        //gira el contexto unos 270deg
        ctx.rotate(3*Math.PI/2);
        // dibuja el trazado
        ctx.beginPath();
            for(i = 0; i < L; i++ ){
                x = X + R * Math.cos( rad*i );
                y = Y + R * Math.sin( rad*i );
                ctx.lineTo(x, y);
            }
         ctx.closePath();
         ctx.fill();
         ctx.stroke();
     }
     data = canvas;
}



function avisos(R,L,data){

    if ((R == "" || L == "")){
        if(data == ""){
            document.getElementById("aviso")
                 .className = document.getElementById("aviso")
                 .className.replace( "alert-info" , "alert-warning" );
            document.getElementById("aviso").innerHTML='No hay imagen que guardar';
        }else{
            document.getElementById("aviso")
                 .className = document.getElementById("aviso")
                 .className.replace( "alert-info" , "alert-warning" );
            document.getElementById("aviso").innerHTML='Datos incorrectos o campos vacios';
        }
    }else if(radio > 300){
        document.getElementById("aviso")
             .className = document.getElementById("aviso")
             .className.replace( "alert-info" , "alert-danger" );
        document.getElementById("aviso").innerHTML='Tamaño demasiado grande para el lienzo';
    }else{
        if(document.getElementById("aviso").className.match(/(?:^|\s)alert-warning/) ){
             document.getElementById("aviso")
             .className = document.getElementById("aviso")
             .className.replace("alert-warning","alert-info")
        }else if(document.getElementById("aviso").className.match(/(?:^|\s)alert-danger/) ){
             document.getElementById("aviso")
             .className = document.getElementById("aviso")
             .className.replace("alert-danger","alert-info")
        }



        document.getElementById("aviso").innerHTML='Elige el tamaño del poligono y la cantidad lados | Con color negro seleccionado los poligonos serán de color aleatorio';
    }
}

function aleatorio(){
    var r = Math.floor(Math.random() * 255) + 70;
    var g = Math.floor(Math.random() * 255) + 70;
    var b = Math.floor(Math.random() * 255) + 70;
    var color = 'rgba(' + r + ',' + g + ',' + b + ', 1)';
    return color;
}




